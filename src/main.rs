#![feature(plugin)]
#![plugin(rocket_codegen)]

#[macro_use]
extern crate askama;
extern crate rocket;

use askama::Template;

use std::io::Cursor;
use std::path::{Path, PathBuf};

use rocket::request::Request;
use rocket::http::{ContentType, Status};
use rocket::local::Client;
use rocket::response::{self, Response, Responder, NamedFile};

#[derive(Template)]
#[template(path = "hello.html")]
struct HelloTemplate<'a> {
    example_str: &'a str,
    example_f64: f64,
}

impl<'r> Responder<'r> for HelloTemplate<'r> {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        Response::build()
            .sized_body(Cursor::new(format!("{}", self.render().unwrap())))
            .header(ContentType::HTML)
            .ok()
    }
}

#[get("/")]
fn hello() -> HelloTemplate<'static> {
    HelloTemplate {
        example_str: "I am a str!",
        example_f64: 283.3993293403
    }
}

#[get("/static/<file..>")]
fn static_files(file: PathBuf) -> Option<NamedFile> {
    NamedFile::open(Path::new("static/").join(file)).ok()
}

fn main() {
    let rocket = rocket::ignite()
        .mount("/", routes![hello, static_files])
        .launch();
}
